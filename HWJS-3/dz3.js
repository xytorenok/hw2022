let limit

do {
  getLimit()
} while (limit % 1 !== 0);



showNumbers()

function getLimit() {
  limit = prompt('Введите число')
}

function showNumbers() {
  if (limit >= 5) {
    // вариант 1
    // for (let i = 5; i <= limit; i += 5) {
    //   console.log(i);
    // }
    // вариант 2
    for (let i = 0; i <= limit; i++) {
      if (i % 5 == 0) {
        console.log(i);
      }
      
    }
  } else {
    alert('Sorry, no numbers')
  }
}
